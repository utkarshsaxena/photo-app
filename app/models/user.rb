class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :payment
  accepts_nested_attributes_for :payment # we define this since we will be accepting user's info along with the payment info on form submission
  has_many :images

end
