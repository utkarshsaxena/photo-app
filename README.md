# README #

## Introduction:  

This is the third summer projects I will be developing using Ruby on Rails. Primary aim of these projects is to focus on learning Ruby on Rails and apply my knowledge to implement complex features in my web application.

Some of the features implemented are:

* Authentication system for signing-up, editing profile and logging out.
* Integrating with Stripe API to allow users to switch to the paid plan and make a payment.
* Allow a user to upload images to AWS S3 bucket.
* Edit the size of the image on the server using Imagemagick.
* Send out email confirmation to user's email account (using SendGrid API) on sign-up to validate a new account.

## Technologies: ##

* HTML/ERB Templating
* CSS/Bootstrap
* Ruby on Rails
* Stripe
* SendGrid
* AWS S3 Bucket


## Purpose: ##

* Become proficient in Ruby on Rails.
* Be well equipped for my Fall '16 internship with Shopify.

## Procedure: ##

1. Install [Ruby on Rails.](http://rubyonrails.org/)
2. Browse to the root folder and run the following command in the terminal: rails server -p 0.0.0.0 or rails server

OR

1. Browse to [the website](https://ultimate-photo-app-utk.herokuapp.com)


## Timestamp: ##

**July, 2016